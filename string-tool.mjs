/**
 * A class for working with strings, such as shortening with ellipsis, finding line and column numbers
 * and related operations.
 */
export default class StringTool {
    /**
     * The regular expression pattern used to classify and capture words from the source string.
     */
    static TOKEN_PATTERN = /(?<number>\p{N}+?)|(?<word>\p{L}+)|(?<whitespace>\p{Z}+)|(?<punctuation>\p{P}+)|(?<symbol>\p{S}+)|(?<mark>\p{M}+)/udy;


    /**
     * 
     * @param {string} string 
     */
    constructor(string) {
        this.string = String(string);
    }

    /**
     * Replace template variables enclosed in curly braces with values according
     * to an object map. Function callbacks can be provided to enable lazy evaluation.
     * 
     * @param {{[keyword: string]: number|string|()=>number|string}} templateVars 
     */
    interpolate(templateVars) {
        return this.string.replace(/{([^}]+)}/g, (m, n)=>{
            const result = n in templateVars ? templateVars[n] : m;
            return String(typeof result === "function" ? result() : result);
        });
    }    

    /**
     * @type {StringCollection}
     */
    get lines() {
        return new StringCollection(this.string.split(/\r\n|\r|\n/));
    }

    /**
     * Trim the string to the max length given, adding ellipsis after if the string was cut.
     * 
     * @param {*} maxLength 
     * @returns 
     */
    ellipsis(maxLength=10) {
        if (this.string.length > maxLength) {
            return this.string.substring(0, maxLength - 1) + "…";
        } else {
            return "" + this.string;
        }
    }

    /**
     * Return the line at the given character offset in the string
     * 
     * @param {number} offset 
     * @returns {string}
     */
    lineAt(offset) {
        const start = this.lineStartOffset(offset), end = this.lineEndOffset(offset);
        return this.string.substring(start, end);
    }

    /**
     * Get the word, symbol or identifier starting at the given character offset 
     * in the string.
     * 
     * @param {number} offset 
     * @returns {{string: string, kind: string}}
     */
    wordAt(offset) {
        if (offset >= this.string.length) {
            return { string: '', kind: 'end-of-file' };
        }

        const regexp = StringTool.TOKEN_PATTERN;
        regexp.lastIndex = offset;
        const matches = regexp.exec(this.string);
        if (matches === null) {
            return { string: this.string.substring(offset, offset+10), kind: "unknown"};
        }

        for (const wordClass in matches.groups) {
            if (matches.groups[wordClass] !== undefined) {
                return { string: matches.groups[wordClass], kind: wordClass };
            }
        }

        return null;
    }

    /**
     * Get the start offset of the line which contains the given offset.
     * 
     * @param {number} offset 
     * @returns {number}
     */
    lineStartOffset(offset) {
        return this.string.substring(0, offset).lastIndexOf("\n") + 1;
    }

    /**
     * Get the end offset of the line which contains the given offset.
     * 
     * @param {number} offset 
     * @returns {number}
     */
    lineEndOffset(offset) {
        const endOffset = this.string.indexOf("\n", offset);
        if (endOffset === -1) {
            return this.string.length;
        }
        return endOffset;
    }

    /**
     * @param {number} offset 
     * @returns {number}
     */
    lineNumber(offset) {
        const slice = this.string.substring(0, offset);
        return slice.split(/\n/).length;
    }

    /**
     * @param {number} offset 
     * @returns {number}
     */
    columnNumber(offset) {
        if (offset < 0) return 0;
        if (offset >= this.length) {
            offset = this.length;
        }
        return offset - this.lineStartOffset(offset) ;
    }

    [Symbol.toPrimitive]() {
        return this.string;
    }
}

/**
 * @class StringCollection
 * @extends {Array<string>}
 */
export class StringCollection extends Array {

    /**
     * All strings that contain ...
     * 
     * @param {string} string 
     */
    hasSubstring(string) {
        return this.filter((value)=>value.indexOf(string) >= 0);
    }

    /**
     * All strings that begin with ...
     * 
     * @param {string} string 
     * @returns 
     */
    startsWith(string) {
        return this.filter((value)=>value.startsWith(string));
    }

    /**
     * All lines that are not empty.
     * 
     * @returns 
     */
    noEmptyLines() {
        return this.filter((value)=>value.trim() !== "");
    }

    toString() {
        return this.join();
    }    
}