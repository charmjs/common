/**
 * Real private properties for objects by using a `WeakMap`.
 * 
 * Usage:
 * 
 * ```
 * // Create a p() function to access private members.
 * const p = privateProperties();
 * 
 * // The p() function is available inside the class through the lexical scope.
 * export class MyClass() {
 *
 *     constructor() {
 *         // Access private properties via the p(this) function
 *         p(this).createdTime = new Date();
 *     }
 * 
 * }
 * ```
 * 
 * @type {() => (this: Object) => Object}
 */
export default function privateProperties() {
    const privs = new WeakMap();

    return (object) => {
        if (!privs.has(object)) {
            privs.set(object, {});
        }
        return privs.get(object);
    };
}
