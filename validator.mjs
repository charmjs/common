/**
 * @typedef {Object} ValidationRules
 * @property {number} [maxLength] Maximum length of the value
 * @property {number} [minLength] Minimum length of the value
 * @property {RegExp} [pattern] Regex pattern to validate
 * @property {"date-time" | "time" | "date" | "duration" | "email" | "idn-email" | "hostname" | "idn-hostname" | "ipv4" | "ipv6" | "uuid" | "uri" | "uri-reference" | "iri" | "iri-reference" | "uri-template" | "json-pointer" | "regex" | "relative-json-pointer"} [format] Predefined formats according to JSON schema
 * @property {number} [minimum] Minimum value
 * @property {number|boolean} [exclusiveMinimum] Minimum value inclusive
 * @property {number} [maximum] Maximum value
 * @property {number|boolean} [exclusiveMaximum] Maximum value inclusive
 * @property {number} [multipleOf] Number must be a multiple of
 * @property {"string"|"number"|"integer"|"boolean"|"null"|("string"|"number"|"integer"|"boolean"|"null")[]} [type] The value type required
 * @property {boolean} [required] Is a value required
 */

import StringTool from "./string-tool.mjs";

/**
 * Validator for the basic types of JSON Schema. Currently complex types
 * such as array or object are not supported.
 */
export default class Validator {

    static DEFAULT_MESSAGES = {
        maxLength: "Maximum length is {criterion}",
        minLength: "Minimum length is {criterion}",
        pattern: "Invalid formatting",
        format: "Expecting a {criterion} formatted string",
        minimum: "Minimum value is {criterion}",
        exclusiveMinimum: "Value must be greater than {criterion}",
        maximum: "Maximum value is {criterion}",
        exclusiveMaximum: "Value must be lesser than {criterion}",
        multipleOf: "Value must be a multiple of {criterion}",
        required: "Required",
        type: "Value is not of type {criterion}",
    };

    /**
     * Defines which rules are applied depending on the value type received.
     *
     * @type {{[type: string]: string[]}}
     */
    static TYPE_TEST_MAP = {
        "string": ["type","maxLength", "minLength", "pattern"],
        "number": ["type","minimum", "maximum", "multipleOf", "exclusiveMinimum", "exclusiveMaximum"],
        "integer": ["type","minimum", "maximum", "multipleOf", "exclusiveMinimum", "exclusiveMaximum"],
        "boolean": ["type"],
        "null": ["type"]
    };

    /**
     * The functions that perform the actual validation of the specified rules.
     * 
     * @type {{[ruleName: string]: (value: any, criterion: any)=>boolean}}
     *
     * @static
     * @memberof Validator
     */
    static TESTER_FUNCTIONS = {
        maxLength: (value, criterion)=>String(value).length <= criterion,
        minLength: (value, criterion)=>String(value).length >= criterion,
        pattern: (value, criterion)=>RegExp(criterion, "y").test(value),
        format: (value, format)=>{
            const strValue = String(value);
            switch (format) {
                case "date":
                    return isOnlyDate(strValue);
                case "date-time":
                    return isDateTime(strValue);
                case "time":
                    return isOnlyTime(strValue);
                case "email":
                    return isValidEmail(strValue);
                case "hostname":
                    return isValidHostName(strValue);
                default:
                    throw new Error("The format rule '" + format + "' is not implemented");
            }    
            return null;
        },
        minimum: (value, criterion)=>value >= criterion,
        exclusiveMinimum: (value, criterion)=>value > criterion,
        maximum: (value, criterion)=>value <= criterion,
        exclusiveMaximum: (value, criterion)=>value < criterion,
        multipleOf: (value, criterion)=>0.0 === value % criterion,
        required: (value, criterion)=>!criterion || (value !== null && value !== undefined && !isNaN(value) && String(value).trim() !== ""),
        type: (value, criterion) => {
            const basic = (value, criterion)=>{
                switch (criterion) {
                    case "string":
                        return typeof value === "string";
                    case "number":
                        if (typeof value === "bigint" || typeof value === "number") {
                            return true;
                        }
                        return false;
                    case "integer":
                        if (typeof value === "bigint") {
                            return true;
                        }
                        if (typeof value === "number" && Math.floor(value) === value) {
                            return true;
                        }
                        return false;
                    case "boolean":
                        return typeof value === "boolean";
                    case "null":
                        return value === null;
                }
            };
            if (typeof criterion === "object" && criterion instanceof Array) {
                for (const type of value) {
                    if (basic(value, type)) {
                        return true;
                    }
                }
                return false;
            } else {
                return basic(value, criterion);
            }
    
        },
    }

    MESSAGES = Object.assign({}, Validator.DEFAULT_MESSAGES);

    /**
     * @param {ValidationRules} rules 
     */
    constructor(rules) {
        /**
         * @private
         */
        this._rules = Object.assign({}, rules);

        // special handling
        if ("exclusiveMaximum" in this._rules && this._rules.exclusiveMaximum === true) {
            this._rules.exclusiveMaximum = this._rules.maximum;
            delete this._rules.maximum;
        }
        if ("exclusiveMinimum" in this._rules && this._rules.exclusiveMinimum === true) {
            this._rules.exclusiveMinimum = this._rules.minimum;
            delete this._rules.minimum;
        }

        for (const rule in this.MESSAGES) {
            if (rule in rules) {
                const message = this.MESSAGES[rule];
                const criterion = rules[rule];
                const testerFunction = Validator.TESTER_FUNCTIONS[rule];
                this[rule] = (value) => {
                    if (testerFunction(value, criterion)) {
                        return null;
                    }
                    const st = new StringTool(message);
                    return st.interpolate({
                        value: String(value),
                        criterion: String(criterion) 
                    });
                };
            } else {
                // Provide stubbed implementations
                this[rule] = (value) => null;
            }
        }
    }

    /**
     * Validate a value according to the rules
     * 
     * @param {*} value 
     * @returns {string|null}
     */
    isInvalid(value) {
        const enabledTests = Validator.TYPE_TEST_MAP[typeof value];
        const strValue = String(value);
        let result;
        if (result = this.required(value)) {
            return result;
        }

        for (const rule in this._rules) {
            if (enabledTests.indexOf(typeof value) < 0) {
                // don't apply the test for values of the given type
                continue;
            }
            if (rule === "required") {
                continue;
            }

            result = this[rule](value);
            if (result !== null) {
                return result;
            }
        }

        return null;
    }
}

/** 
 * Validate a value according to a set of basic validation rules. Returns an error
 * string or null if nothing was wrong.
 * 
 * Usage:
 * ```
 * // Declare rules
 * const validationRules = {
 * }
 * 
 * @param {string|number|boolean|null|undefined} value 
 * @param {ValidationRules} rules 
 * @returns {string|null}
 */
export default function isInvalid(value, rules) {
    const validator = new Validator(rules);
    return validator.isInvalid(value);
};

/**
 * @see https://stackoverflow.com/a/46181/1667011
 * 
 * @param {*} email 
 * @returns {boolean}
 */
function isValidEmail(email) {
    return String(email)
        .toLowerCase()
        .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
}

function isValidHostName(host) {
    const strVal = String(host).toLowerCase();

    return /^([[:alnum:]][[:alnum:]\-]{0,61}[[:alnum:]]|[[:alpha:]])$/.test(strVal) && /^.*[[:^digit:]].*$/.test(strVal)
}

function isOnlyDate(d) {
    const date = new Date(d + "T00:00:00+00:00");
    return date instanceof Date && !!date.getDate();
}
function isOnlyTime(d) {
    const date = new Date("2020-01-01T" + d);
    return date instanceof Date && !!date.getDate();
}
function isDateTime(d) {
    const date = new Date(d);
    return date instanceof Date && !isOnlyDate(d) && !isOnlyTime(d) && !!date.getDate();
};
