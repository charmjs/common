CharmJS Common
==============

A small collection of useful classes shared between CharmJS libraries.

class StringTool
----------------

Manipulate and extract basic information from strings.

Examples:

```
const someString = "Hello World\nLine number 2";
const st = new StringTool(someString);

console.log(st.lineNumber(10));         // 0
console.log(st.columnNumber(15));       // 1
console.log(st.lineAt(10));             // "Hello World"
console.log(st.lineAt(15));             // "Line number 2"
console.log(st.lineStartOffset(15));    // 12
console.log(st.wordAt(12));             // { kind: "word", string: "Line" }
```

class Validator()
-----------------

JSON Schema inspired validation.

Examples:
```
const usernameValidator = new Validator({
    minLength: 3,
    maxLength: 30,
    required: true,
    pattern: /[a-z][a-z0-9_]/
});

let reason;
if (reason = usernameValidator.isInvalid("my-username")) {
    console.log("Invalid username:", reason);
}
```

function privateProperties()
-----------------------------

Attach values to objects using WeakMaps, for example to create private properties
which are truly private.

Example:
```
// Create an accessor function
const p = privateProperties();

class User {
    constructor(username, password) {
        this.username = username;

        // this property will be a true private property.
        p(this).password = password;
    }
}
```